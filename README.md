# Ejemplo de uso de operaciones bitwise
Implementa las funciones:
```
/*
* Retorna verdadero si el bit de mimascara en la posicion pos es 1. La funci�n asume que el primer bit es posici�n 0.
*/
bool isBitSet(unsigned char valor, int pos);

/*
* La funci�n llena el arreglo misbytes con cada byte de valor. Asume que misbytes es un arreglo de 4 unsigned char.
*/
void returnArrayBytes(unsigned int valor, unsigned char *misbytes);
```

# Uso
Para probar isBitSet:
```
./mimascara -b
```
Para probar returnArrayBytes:
```
./mimascara -B
```