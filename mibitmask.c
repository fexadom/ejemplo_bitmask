#include<stdio.h>
#include<stdbool.h>
#include<getopt.h>

/*
* La funci�n llena el arreglo misbytes con cada byte de "valor". Asume que "misbytes" es un arreglo de 4 unsigned char.
*/
void returnArrayBytes(unsigned int valor, unsigned char *misbytes)
{
	//Almacena en modo Big-endian
	misbytes[0] = (valor >> 24); //MSB
	misbytes[1] = (valor >> 16) & 0xff;
	misbytes[2] = (valor >> 8) & 0xff;
	misbytes[3] = valor & 0xff; //LSB
}

/*
* Retorna verdadero si el bit de "mimascara" en la posicion "pos" es 1. La funci�n asume que el primer bit es posici�n 0.
*/
bool isBitSet(unsigned char valor, int pos)
{
	unsigned char bitmask = (1 << pos);

	return valor & bitmask;
}

int main(int argc, char *argv[])
{

	int opt;
	unsigned int valor, pos;
	unsigned char misbytes[4];

	while((opt = getopt(argc, argv, "bB")) != -1)
	{
		switch(opt)
		{
			case 'b':
				printf("Ingrese valor de un byte: ");
				scanf("%d", &valor);
				printf("Ingrese posicion: ");
				scanf("%d", &pos);
				if(isBitSet((unsigned char) valor,pos))
					printf("El bit %d esta en 1 (is set)\n",pos);
				else
					printf("El bit %d esta en 0 (is not set)\n",pos);
			break;
			case 'B':
				printf("Ingrese entero: ");
				scanf("%d", &valor);
				returnArrayBytes(valor, misbytes);
				printf("Bytes son: 0x%02x 0x%02x 0x%02x 0x%02x\n", misbytes[0], misbytes[1], misbytes[2], misbytes[3]);
				break;
			default:
				fprintf(stderr,"Test desconocido, usar -b o -B\n");
		}
	}

	if(argc < 2)
		fprintf(stderr,"Usar -b o -B\n");
}
